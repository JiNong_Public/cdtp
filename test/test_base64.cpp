/**
 * Copyright © 2016-2018 JiNong Inc.
 * All Rights Reserved.
 *
 * \file test_base64.cpp
 * \brief Base64 Test
 *
 */

#include "test.h"
#include "base64.h"

int
test_base64() {
	unsigned char src[] = "abcefghij";
	char *dest;
	unsigned char rsrc[10];
	int len;

	printf ("src : %s %d\n", src, (int)strlen((char *)src));
	if (Base64::encode(src, strlen((char *)src), &dest) < 0) {
		printf ("memory allocation problem.\n");
		return -1;	// memory allocation problem
	}

	printf ("dest : %s\n", dest);
	if (Base64::decode(dest, rsrc, 6) > 0) {
		printf ("should fail because 6 is not enough.\n");
		return -1; // fail because 6 is not enough
    }

	len = Base64::decode(dest, rsrc, 9);
	if (len < 0) {
		printf ("should success because 9 is enough.\n");
		return -1; // fail because 9 is enough
    }

	rsrc[9] = '\0';
	printf ("ret : %s %d\n", rsrc, len);

	if (memcmp((void *)src, (void *)rsrc, len) == 0) {
		return 0;
    } else {
		printf ("decoded is not same. %s %s.\n", src, rsrc);
		return -1;
    }
}

int
main (int argc, char *argv[]) {
	TEST(test_base64());
	return 0;
}
