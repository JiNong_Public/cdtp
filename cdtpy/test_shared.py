#-*- coding: utf-8 -*-
"""
 * Copyright © 2016-2018 JiNong Inc.
 * All Rights Reserved.
 *
"""

import ctypes
import platform

if platform.system() == 'Linux':
    CDTP_SO_PATH = '../build/lib/libcdtp.so'
elif platform.system() == 'Darwin':
    CDTP_SO_PATH = '../build/lib/libcdtp.dylib'
else:
    # 현재 윈도우 및 다른 OS를 지원하지 않습니다.
    CDTP_SO_PATH = ''

cdtp = ctypes.cdll.LoadLibrary(CDTP_SO_PATH)

GWKEY = "#1234"
cdtp._SetSNID(1)
cdtp._Encode.restype = ctypes.c_char_p
cdtp.Req_GetGatewayKey.restype = ctypes.c_char_p
req = cdtp._GetRegistrationRequest(GWKEY, 60)
coded = cdtp._Encode(req)
print coded
tmp = coded
print tmp
cdtp.Req_Free(req)

req = cdtp._DecodeAsRequest(tmp)
print cdtp.Msg_GetSNID(req)
print cdtp.Msg_GetSequence(req)
print cdtp.Req_GetGatewayKey(req)
print cdtp.Req_GetSleepTime(cdtp._DecodeAsRequest(coded))

res = cdtp._GetResponse(req, 0)
rescode = cdtp._Encode(res)
print rescode

cdtp.Req_Free(req)
cdtp.Res_Free(res)

