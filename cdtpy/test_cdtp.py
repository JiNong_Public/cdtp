#-*- coding: utf-8 -*-
"""
 * Copyright © 2016-2018 JiNong Inc.
 * All Rights Reserved.
 *
"""

import base64
import cdtp

client = cdtp.CDTPClient("JN-01", 1, 60)
regreq = client.register()
print "Client Registration", client.encode(regreq)
print "len ", len(regreq)
decstr = ''.join(format(x, '02x') for x in regreq)
print len(decstr), decstr

server = cdtp.CDTPServer("#1237")
parsed = server.parse(regreq)
print "Server Parse Registration should fail", parsed

server = cdtp.CDTPServer("JN-01")
parsed = server.parse(regreq)
print "Server Parse Registration", parsed

res = server.response(parsed, cdtp.CDTP_OK)
print "Server Resonpse", res

print "Client Registered?", client.isregistered(res)

noti = client.setobservations({1:10, 2:20, 3:30})
print "Client Notification", noti
dec = base64.b64decode(noti)
print "decode "
decstr = ''.join(format(ord(x), '02x') for x in dec)
print len(decstr), decstr

parsed = server.parse(noti)
print "server Parse Notification", parsed


