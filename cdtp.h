/**
 * Copyright © 2016-2018 JiNong Inc.
 * All Rights Reserved.
 *
 * \file cdtp.h
 * \brief 스마트팜 센서 노드와 게이트웨이간 비연결형 통신 프로토콜
 * Connectionless Devices Transmission Protocol
 */

#ifndef _CDTP_H_
#define _CDTP_H_

#include "includes/cdtp_core.h"

class CDTP {
protected:
    static cdtpmsgtype_t Decode(char *pcoded) {
        return _Decode(pcoded);
    }

public:
    static void SetSNID(unsigned char snid) {
        _SetSNID(snid);
    }

    static unsigned char GetSNID() {
        return _GetSNID();
    }

    static char *Encode(Message *pmsg) {
        return _Encode(pmsg);
    }

    static char *EncodeS(Message *pmsg) {
        return _EncodeS(pmsg);
    }

    static Request *DecodeAsRequest(char *pcoded) {
        return _DecodeAsRequest(pcoded);
    }

    static Response *DecodeAsResponse(char *pcoded) {
        return _DecodeAsResponse(pcoded);
    }

    static Notification *DecodeAsNotification(char *pcoded) {
        return _DecodeAsNotification(pcoded);
    }

    static Notification *GetNotification() {
        return _GetNotification();
    }

    static Request *GetRegistrationRequest(unsigned char *gwkey, int sleeptime) {
        return _GetRegistrationRequest(gwkey, sleeptime);
    }

    static Request *GetConfigurationRequest(unsigned char snid, unsigned char *gwkey, unsigned char *newgwkey, int sleeptime) {
        return _GetConfigurationRequest(snid, gwkey, newgwkey, sleeptime);
    }

    static Response *GetResponse(Request *preq, cdtprescode_t code) {
        return _GetResponse(preq, code);
    }
};

#endif

