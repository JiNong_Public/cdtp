/**
 * Copyright © 2016-2018 JiNong Inc.
 * All Rights Reserved.
 *
 * \file test_shared.cpp
 * \brief CDTP shared library Test
 *
 */

#include <dlfcn.h>
#include <iostream>

#include "test.h"
#include "../cdtp.h"

using namespace std;

#ifdef __APPLE__
  #define LIBRARYPATH "../lib/libcdtp.dylib"
#elif __linux__
  #define LIBRARYPATH "../lib/libcdtp.so"
#else
  // 현재 윈도우 지원은 하지 않습니다.
  #define LIBRARYPATH ""
#endif

int 
test_shared () {
    void* handle = dlopen(LIBRARYPATH, RTLD_LAZY);

    void (*setsnid)(unsigned char);
    unsigned char (*getsnid)();
    
    setsnid = (void (*)(unsigned char))dlsym(handle, "_SetSNID");
    getsnid = (unsigned char (*)())dlsym(handle, "_GetSNID");

    setsnid(1);
    unsigned char ret = getsnid();
    printf ("%d\n", ret);

    dlclose(handle);

    return 0;
}

int
main (int argc, char *argv[]) {
    TEST(test_shared());
    return 0;
}
