/**
 * Copyright © 2016-2018 JiNong Inc.
 * All Rights Reserved.
 *
 * \file cdtp_core.h
 * \brief 스마트팜 센서 노드와 게이트웨이간 비연결형 통신 프로토콜 코어부
 * Connectionless Devices Transmission Protocol
 */

#ifndef _CDTP_CORE_H_
#define _CDTP_CORE_H_

#include "cdtp_message.h"

extern "C" {
    static unsigned char _snid;     ///< 관리 편의를 위한 내부 센서노드 아이디. 클라이언트에서 활용.

    /**
     * 메시지를 파싱한다.
     * @param pmsg 메시지
     * @return 메시지 타입
     */
    cdtpmsgtype_t _Parse(unsigned char *pmsg) {
        memcpy (&(msgbase._header), pmsg, _CDTP_MSGLEN_);
        return (cdtpmsgtype_t)(pmsg[CDTP_HD_MSGTYPE]);
    }

    /**
     * 인코딩된 메시지를 디코딩한다.
     * @param pcoded 인코딩된 메시지
     * @return 메시지 타입
     */
    cdtpmsgtype_t _Decode(char *pcoded) {
        unsigned char msg[_CDTP_MSGLEN_];

        if (Base64::decode(pcoded, msg, _CDTP_MSGLEN_) > 0) {
            return _Parse(msg);
        }
        return CDTP_MT_NONE;
    }

    /**
     * 내부 센서노드 아이디를 세팅한다.
     * @param snid 세팅할 센서노드 아이디
     */
    void _SetSNID(unsigned char snid) {
        _snid = snid;
    }

    /**
     * 내부 센서노드 아이디를 얻는다.
     * @return 내부 센서노드 아이디
     */
    unsigned char _GetSNID() {
        return _snid;
    }

    /**
     * 메시지를 인코딩 한다.
     * @param pmsg 인코딩할 메시지에 대한 포인터
     * @return 인코딩된 메시지. 삭제해줘야 함.
     */
    char *_Encode(Message *pmsg) {
        char *encoded;
        
        if (Base64::encode (pmsg->GetHeader(), _CDTP_MSGLEN_, &encoded) > 0)
            return encoded;
        return nullptr;
    }

    /**
     * 바이너리 메시지를 인코딩 한다.
     * @param pbin 인코딩할 바이너리 메시지에 대한 포인터
     * @return 인코딩된 메시지
     */
    char *_EncodeBinary(unsigned char *pbin) {
        char *encoded;
        
        if (Base64::encode (pbin, _CDTP_MSGLEN_, &encoded) > 0)
            return encoded;
        return nullptr;
    }

    /**
     * 메시지를 인코딩 하고, 내부 정적변수에 담아서 리턴한다. 
     * 최종 인코딩된 메시지만 담고 있어서 주의가 필요하다.
     * @param pmsg 인코딩할 메시지에 대한 포인터
     * @return 인코딩된 메시지
     */
    char *_EncodeS(Message *pmsg) {
        static char buf[_CDTP_ENCODE_MSGLEN_ + 1];
        char *encoded;
        
        if (Base64::encode (pmsg->GetHeader(), _CDTP_MSGLEN_, &encoded) > 0) {
            strcpy(buf, encoded);
            delete encoded;   
            return buf;
        }
        return nullptr;
    }

    /**
     * 메시지를 내부 정적변수에 담아서 리턴한다. 
     * 최종 메시지만 담고 있어서 주의가 필요하다.
     * @param pmsg 메시지에 대한 포인터
     * @return 메시지
     */
    int _GetBinaryS(Message *pmsg, unsigned char *pbin) {
        if (pmsg == nullptr || pbin == nullptr)
            return 0;

        if (pmsg->GetHeader() == nullptr)
            return 0;
     
        memcpy (pbin, pmsg->GetHeader(), _CDTP_MSGLEN_);
        return _CDTP_MSGLEN_;
    }

    /**
     * 인코딩된 메시지를 요청메시지로 디코딩 한다.
     * @param pcoded
     * @return Request
     */
    Request *_DecodeAsRequest(char *pcoded) {
        if (_Decode(pcoded) == CDTP_MT_REQUEST)
            return new Request();
        return nullptr;
    }

    /**
     * 메시지를 요청메시지로 파싱 한다.
     * @param pmsg
     * @return Request
     */
    Request *_ParseAsRequest(unsigned char *pmsg) {
        if (_Parse(pmsg) == CDTP_MT_REQUEST)
            return new Request();
        return nullptr;
    }

    /**
     * 메시지 시퀀스를 얻는다.
     * @param msg 메시지에 대한 포인터
     * @return 메시지 시퀀스 값
     */
    unsigned char Msg_GetSequence(Message *msg) {
        return msg->GetSequence();
    }

    /**
     * 메시지로 부터 센서노드아이디를 얻는다.
     * @param msg 메시지에 대한 포인터
     * @return 메시지 센서노드아이디 값
     */
    unsigned char Msg_GetSNID(Message *msg) {
        return msg->GetSNID();
    }

    /**
     * 요청메시지로부터 게이트웨이 키를 얻는다.
     * @param req 요청메시지에 대한 포인터
     * @return 요청 메시지의 게이트웨이 키
     */
    char *Req_GetGatewayKey(Request *req) {
        static char buf[_CDTP_GWKEY_LEN_ + 1];
        if (req->GetGatewayKey(buf, _CDTP_GWKEY_LEN_ + 1) < 0)
            return nullptr;
        return buf;
    }

    /**
     * 요청메시지로부터 슬립시간을 얻는다.
     * @param req 요청메시지에 대한 포인터
     * @return 요청 메시지의 슬립시간(초)
     */
    int Req_GetSleepTime(Request *req) {
        return req->GetSleepTime();
    }

    /**
     * 요청메시지로부터 새로운 게이트웨이 키를 얻는다.
     * @param req 요청메시지에 대한 포인터
     * @return 요청 메시지의 새로운 게이트웨이 키
     */
    char *Req_GetNewGatewayKey(Request *req) {
        static char buf[_CDTP_GWKEY_LEN_ + 1];
        if (req->GetNewGatewayKey(buf, _CDTP_GWKEY_LEN_ + 1) < 0)
            return nullptr;
        return buf;
    }

    /**
     * 요청메시지로부터 표준 버전을 얻는다.
     * @param req 요청메시지에 대한 포인터
     * @return 요청 메시지의 버전
     */
    unsigned char Req_GetVersion(Request *req) {
        return req->GetVersion();
    }

    /**
     * 응답 메시지로 디코딩 한다.
     * @param pcoded 인코딩된 메시지 
     * @return 응답 메시지
     */
    Response *_DecodeAsResponse(char *pcoded) {
        if (_Decode(pcoded) == CDTP_MT_RESPONSE)
            return new Response();
        return nullptr;
    }

    /**
     * 메시지를 응답메시지로 파싱 한다.
     * @param pmsg
     * @return 응답 메시지
     */
    Response *_ParseAsResponse(unsigned char *pmsg) {
        if (_Parse(pmsg) == CDTP_MT_REQUEST)
            return new Response();
        return nullptr;
    }

    /**
     * 알림 메시지로 디코딩 한다.
     * @param pcoded 인코딩된 메시지 
     * @return 알림 메시지
     */
    Notification *_DecodeAsNotification(char *pcoded) {
        if (_Decode(pcoded) == CDTP_MT_NOTIFICATION)
            return new Notification();
        return nullptr;
    }

    /**
     * 알림 메시지로 파싱 한다.
     * @param pmsg 메시지 
     * @return 알림 메시지
     */
    Notification *_ParseAsNotification(unsigned char *pmsg) {
        if (_Parse(pmsg) == CDTP_MT_NOTIFICATION)
            return new Notification();
        return nullptr;
    }

    /**
     * 알림 메시지로부터 센서(관측치)의 개수를 얻는다.
     * @param 알림 메시지에 대한 포인터
     * @return 센서(관측치)의 개수. 최대 13임.
     */
    int Noti_GetNumberofSensors(Notification *noti) {
        return noti->GetNumberofSensors();
    }

    /**
     * 알림 메시지에서 인덱스에 해당하는 센서아이디를 얻는다.
     * @param 알림 메시지에 대한 포인터
     * @param 인덱스 값
     * @return 센서아이디
     */
    unsigned char Noti_GetSensorID(Notification *noti, int idx) {
        return noti->GetSensorID(idx);
    }

    /**
     * 알림 메시지에서 인덱스에 해당하는 관측치를 얻는다.
     * @param 알림 메시지에 대한 포인터
     * @param 인덱스 값
     * @return 센서 관측치
     */
    unsigned int Noti_GetObservation(Notification *noti, int idx) {
        return noti->GetObservation(idx);
    }

    /**
     * 알림 메시지를 생성한다.
     * @return 새로운 알림 메시지에 대한 포인터
     */
    Notification *_GetNotification() {
        return new Notification(_GetSNID());
    }

    /**
     * 알림 메시지에 새로운 관측치를 추가한다.
     * @param noti 알림 메시지에 대한 포인터
     * @param sensorid 추가할 센서 아이디
     * @param value 추가할 센서 관측치
     * @return 추가 결과. 0이면 성공. 13개 이상 넣을 수 없음.
     */
    cdtprescode_t Noti_AddObservation(Notification *noti, unsigned char sensorid, unsigned int value) {
        return noti->AddObservation(sensorid, value);
    }

    /**
     * 등록을 위한 요청 메시지를 생성한다.
     * @param gwkey 게이트웨이 키
     * @param sleeptime 슬립시간(초)
     * @return 등록을 위한 요청 메시지의 포인터
     */
    Request *_GetRegistrationRequest(unsigned char *gwkey, int sleeptime) {
        Request *req = new Request(_GetSNID());
        req->SetRegistration(gwkey, sleeptime);
        return req;
    }

    /**
     * 재설정을 위한 요청 메시지를 생성한다.
     * @param snid 센서노드 아이디
     * @param gwkey 게이트웨이 키
     * @param newgwkey 새로운 게이트웨이 키
     * @param sleeptime 슬립시간(초)
     * @return 등록을 위한 요청 메시지의 포인터
     */
    Request *_GetConfigurationRequest(unsigned char snid, unsigned char *gwkey, unsigned char *newgwkey, int sleeptime) {
        Request *req = new Request(snid);
        req->SetConfiguration(gwkey, newgwkey, sleeptime);
        return req;
    }

    /**
     * 커스텀 요청 메시지를 생성한다.
     * @param msgcode 메시지 코드
     * @param snid 센서노드 아이디
     * @param body 메시지 바디
     * @return 커스텀 요청 메시지의 포인터
     */
    Request *_GetCustomRequest(cdtpmsgcode_t msgcode, unsigned char snid, unsigned char *body) {
        Request *req = new Request(snid);
        req->SetMsgCode(msgcode);
        req->SetBody(body);
        return req;
    }

    /**
     * 응답 메시지를 생성한다.
     * @param preq 응답할 요청 메시지에 대한 포인터
     * @param code 응답 결과
     * @return 생성된 응답 메시지에 대한 포인터
     */
    Response *_GetResponse(Request *preq, cdtprescode_t code) {
        return new Response(preq, code);
    }

    /**
     * 응답 메시지에서 결과 코드를 얻는다.
     * @param res 응답 메시지에 대한 포인터
     * @return 응답 결과값
     */
    int Res_GetRescode(Response *res) {
        return res->GetRescode();
    }

    /**
     * 요청 메시지를 메모리에서 해제한다.
     * @param 요청 메시지에 대한 포인터
     */
    void Req_Free(Request *req) {
        delete req;
    }

    /**
     * 응답 메시지를 메모리에서 해제한다.
     * @param 응답 메시지에 대한 포인터
     */
    void Res_Free(Response *res) {
        delete res;
    }

    /**
     * 알림 메시지를 메모리에서 해제한다.
     * @param 알림 메시지에 대한 포인터
     */
    void Noti_Free(Notification *noti) {
        delete noti;
    }

    /**
     * 메모리에서 해제한다.
     * @param 포인터
     */
    void _Free(void *ptr) {
        delete ptr;
    }
};

#endif

