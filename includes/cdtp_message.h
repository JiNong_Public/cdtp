/**
 * Copyright © 2016-2018 JiNong Inc.
 * All Rights Reserved.
 *
 * \file cdtp_message.h
 * \brief 스마트팜 센서 노드와 게이트웨이간 비연결형 통신 프로토콜의 메세지 정의
 * Message Definition for CDTP
 */

#ifndef _CDTP_MESSAGE_H_
#define _CDTP_MESSAGE_H_

#include "base64.h"

/** CDTP 메시지 길이 */
#define _CDTP_MSGLEN_            48
/** CDTP 인코딩 메시지 길이 */
#define _CDTP_ENCODE_MSGLEN_     64
/** CDTP 메시지 해더 길이 */
#define _CDTP_HEADERLEN_         8
/** CDTP 메시지 바디 길이 */
#define _CDTP_BODYLEN_           40
/** CDTP 메시지에 포함될 수 있는 최대 센서 수 */
#define _CDTP_NUMOFSEN_          13

/** 메시지 기본 자료 구조 */
typedef struct _msgfmt {
    unsigned char _header[_CDTP_MSGLEN_];   ///< 해더 파트. 실제 메시지 전체
    unsigned char *_body = _header + _CDTP_HEADERLEN_;  ///< 바디 파트. 실제로는 전체 메시지의 바디부분에 대한 포인터 
} cdtpmsg_t;

/** 디폴트로 사용되는 메시지 변수. 성능 향상과 사용메모리 감소를 위한 트릭 */
cdtpmsg_t msgbase;

/** 메시지 코드에 대한 enum */
enum CDTPMSGCODE {
    CDTP_MC_NONE= 0,          ///< 메시지 코드 없음
    CDTP_MC_REGISTRATION,     ///< 등록 메시지 코드
    CDTP_MC_OBSERVATION,      ///< 관측치 메시지 코드
    CDTP_MC_CONFIGURATION,    ///< 설정 메시지 코드
    CDTP_MC_EXTENSION = 100   ///< 추후 확장을 위한 메시지 코드
};

/** 메시지 코드를 위한 데이터 타입 - 메시지 코드 enum 이외의 값을 수용할 수 있도록 처리 */
typedef unsigned char cdtpmsgcode_t;

/** 메시지 타입에 대한 enum 데이터 타입 */
typedef enum {
    CDTP_MT_NONE = 0,         ///< 메시지 타입 없음
    CDTP_MT_REQUEST,          ///< 요청 메시지 타입
    CDTP_MT_RESPONSE,         ///< 응답 메시지 타입
    CDTP_MT_NOTIFICATION      ///< 노티 메시지 타입
} cdtpmsgtype_t;

/** 메시지 해더 위치 데이터 타입 */
enum CDTPHEADERPOS {
    CDTP_HD_MSGCODE = 0,      ///< 메시지 코드 위치
    CDTP_HD_MSGTYPE = 1,      ///< 메시지 타입 위치
    CDTP_HD_SNID = 2,         ///< 센서노드아이디 위치
    CDTP_HD_SEQ = 3,          ///< 메시지 시퀀스 위치
    CDTP_HD_RESCODE = 4,      ///< 응답 코드 위치
    CDTP_HD_RESERVED1 = 4,    ///< 추후 예약 위치 #1 (응답 코드 위치와 동일)
    CDTP_HD_RESERVED2 = 5,    ///< 추후 예약 위치 #2
    CDTP_HD_RESERVED3 = 6,    ///< 추후 예약 위치 #3
    CDTP_HD_RESERVED4 = 7     ///< 추후 예약 위치 #4
} ;

/** 메시지 응답 코드 데이터 타입 */
typedef enum {
    CDTP_RC_OK = 0,           ///< OK 응답코드
    CDTP_RC_ERR,              ///< ERR 응답코드
} cdtprescode_t;

/** 디폴트 센서 아이디 - 배터리 잔량 */
#define _CDTP_SN_BATTERY_        0xFB
/** 예약 센서 아이디 #1 */
#define _CDTP_SN_RESERVED1_   0xFC
/** 예약 센서 아이디 #2 */
#define _CDTP_SN_RESERVED2_   0xFD
/** 예약 센서 아이디 #3 */
#define _CDTP_SN_RESERVED3_   0xFE
/** 예약 센서 아이디 #4 */
#define _CDTP_SN_RESERVED4_   0xFF

/** 메시지 기본 클래스 - 가상클래스 임 */
class Message {
protected:
    cdtpmsg_t *_pmsg;     ///< 메시지 처리를 위한 내부 버퍼 포인터

public:
    /** 메시지 기본 생성자 */
    Message() {
        _pmsg = &msgbase;   // 디폴트로 내부 버퍼를 직접 연결함. 이 경우 메시지가 덮여 써지기 때문에 메시지 사용시 주의가 필요함. 
    }

    /** 메시지 기본 생성자 - 내부 버퍼를 별도로 사용하는 경우에 사용 */ 
    Message (cdtpmsg_t *pmsg, unsigned char snid) {
        _pmsg = pmsg;
        (_pmsg->_header)[CDTP_HD_SNID] = snid;
        (_pmsg->_header)[CDTP_HD_SEQ] = 0;
        NextSeq ();
    }

    /** 메시지 기본 생성자 - 내부 버퍼는 공통으로 사용 */
    Message (unsigned char snid) {
        _pmsg = &msgbase;
        (_pmsg->_header)[CDTP_HD_SNID] = snid;
        (_pmsg->_header)[CDTP_HD_SEQ] = 0;
        NextSeq ();
    }

    virtual ~Message () {}

    /**
     * 메시지 코드를 획득한다.
     * @return 메시지 코드 타입 
     */
    cdtpmsgcode_t GetMsgCode() {
        return (cdtpmsgcode_t)((_pmsg->_header)[CDTP_HD_MSGCODE]);
    }

    /**
     * 메시지 코드를 세팅한다.
     * @param msgcode 메시지 코드 타입 
     */
    void SetMsgCode(cdtpmsgcode_t msgcode) {
        (_pmsg->_header)[CDTP_HD_MSGCODE] = (unsigned char) msgcode;
    }

    /**
     * 메시지 타입을 획득한다.
     * @return 메시지 타입 
     */
    cdtpmsgtype_t GetMsgType() {
        return (cdtpmsgtype_t)((_pmsg->_header)[CDTP_HD_MSGTYPE]);
    }

    /**
     * 메시지 타입을 세팅한다.
     * @param msgtype 메시지 타입 
     */
    void SetMsgType(cdtpmsgtype_t msgtype) {
        (_pmsg->_header)[CDTP_HD_MSGTYPE] = (unsigned char) msgtype;
    }

    /**
     * SNID를 세팅한다.
     * @param snid SNID
     */
    void SetSNID(unsigned char snid) {
        (_pmsg->_header)[CDTP_HD_SNID] = snid;
    }

    /**
     * SNID를 읽어온다.
     * @return unsigned char SNID값
     */
    unsigned char GetSNID() {
        return (_pmsg->_header)[CDTP_HD_SNID];
    }

    /**
     * Header를 가져온다.
     * @return unsigned char * 해더에 대한 포인터
     */
    unsigned char *GetHeader() {
        return _pmsg->_header;
    }

    /**
     * Body를 가져온다.
     * @return unsigned char * 바디에 대한 포인터
     */
    unsigned char *GetBody() {
        return _pmsg->_body;
    }

    /**
     * Body를 가져온다.
     * @return unsigned char * 바디에 대한 포인터
     */
    void SetBody(unsigned char *body) {
        memcpy(_pmsg->_body, body, _CDTP_BODYLEN_);
    }

    /**
     * 시퀀스를 가져온다.
     * @return unsigned char 시퀀스 값
     */
    unsigned char GetSequence() {
        return (_pmsg->_header)[CDTP_HD_SEQ];
    }

    /**
     * 메시지 시퀀스를 다음 시퀀스 값으로 변경한다.
     */
    void NextSeq() {
        if ((_pmsg->_header)[CDTP_HD_SEQ] == 0xFF)
            (_pmsg->_header)[CDTP_HD_SEQ] = 0;
        else
            (_pmsg->_header)[CDTP_HD_SEQ] = (_pmsg->_header)[CDTP_HD_SEQ] + 1;
    }

    /**
     * 현재 사용할 일은 없겠지만 예약해더필드를 세팅한다.
     */
    void SetReserved(unsigned char reserved[]) {
        (_pmsg->_header)[CDTP_HD_RESERVED1] = reserved[0];
        (_pmsg->_header)[CDTP_HD_RESERVED2] = reserved[1];
        (_pmsg->_header)[CDTP_HD_RESERVED3] = reserved[2];
        (_pmsg->_header)[CDTP_HD_RESERVED4] = reserved[3];
    }
};

/** CDTP 게이트웨이 키의 길이 */
#define _CDTP_GWKEY_LEN_      5
/** CDTP 표준 1의 버전 */
#define _CDTP_VERSION_1        0x01
/** CDTP 최종 표준 버전 */
#define CDTP_VERSION            _CDTP_VERSION_1

/** 요청 메시지 클래스 */
class Request : public Message {
public:
    /** 요청 메시지 생성자 */
    Request () : Message () {
    }

    /** 요청 메시지 생성자 - 내부 버퍼를 직접 지정하는 경우 */
    Request (cdtpmsg_t *pmsg, unsigned char snid) : Message(pmsg, snid){
        SetMsgType (CDTP_MT_REQUEST);
    }

    /** 요청 메시지 생성자 */
    Request (unsigned char snid) : Message (snid) {
        SetMsgType (CDTP_MT_REQUEST);
    }

    ~Request () {
    }

    /** 
     * 요청 메시지를 설정 요청으로 세팅한다.
     * @param gwkey 현재 게이트웨이 키
     * @param newgwkey 새로운 게이트웨이 키
     * @param sleep 슬립타임(초)
     */
    void SetConfiguration (unsigned char *gwkey, unsigned char *newgwkey, int sleep) {
        SetReqBody (gwkey, sleep, newgwkey);
        SetMsgCode (CDTP_MC_CONFIGURATION);
    }

    /** 
     * 요청 메시지를 등록 요청으로 세팅한다.
     * @param gwkey 현재 게이트웨이 키
     * @param sleep 슬립타임(초)
     */
    void SetRegistration (unsigned char *gwkey, int sleep) {
        SetReqBody(gwkey, sleep, nullptr);
        SetMsgCode (CDTP_MC_REGISTRATION);
    }

    /** 
     * 메시지 바디를 세팅한다.
     * @param gwkey 현재 게이트웨이 키
     * @param newgwkey 새로운 게이트웨이 키
     * @param sleep 슬립타임(초)
     */
    void SetReqBody (unsigned char *gwkey, int sleep, unsigned char *newgwkey) {
        memcpy (_pmsg->_body, gwkey, _CDTP_GWKEY_LEN_);
        (_pmsg->_body)[_CDTP_GWKEY_LEN_] = CDTP_VERSION;
        (_pmsg->_body)[_CDTP_GWKEY_LEN_ + 1] = (unsigned char) (sleep >> 8);
        (_pmsg->_body)[_CDTP_GWKEY_LEN_ + 2] = (unsigned char) (sleep & 0xFF);
        if (newgwkey != nullptr) {
            memcpy (_pmsg->_body + (_CDTP_GWKEY_LEN_ + 3), newgwkey, _CDTP_GWKEY_LEN_);
        }
    }

    /**
     * 메시지버퍼로 게이트웨이 키를 꺼낸다.
     * @param buf 게이트웨이키 버퍼
     * @param bufsize 버퍼 사이즈
     * @return 성공이면 0, 실패면 음수
     */
    int GetGatewayKey(char *buf, int bufsize) {
        if (bufsize <= _CDTP_GWKEY_LEN_)
            return -1;
        memcpy(buf, _pmsg->_body, _CDTP_GWKEY_LEN_);
        buf[_CDTP_GWKEY_LEN_] = '\0';
        return 0;
    }

    /**
     * 슬립시간을 꺼낸다.
     * @return 슬립시간(초)
     */
    int GetSleepTime() {
        int stime;
        stime = (_pmsg->_body)[_CDTP_GWKEY_LEN_ + 1] << 8 ;
        stime += (_pmsg->_body)[_CDTP_GWKEY_LEN_ + 2];
        return stime;
    }

    /**
     * 메시지에서 버전을 꺼낸다.
     * @return 버전(0x01)
     */
    unsigned char GetVersion() {
        return (_pmsg->_body)[_CDTP_GWKEY_LEN_];
    }

    /**
     * 메시지버퍼로 새 게이트웨이 키를 꺼낸다.
     * @param buf 게이트웨이키 버퍼
     * @param bufsize 버퍼 사이즈
     * @return 성공이면 0, 실패면 음수
     */
    int GetNewGatewayKey(char *buf, int bufsize) {
        if (GetMsgCode() != CDTP_MC_CONFIGURATION)
            return -2;
        if (bufsize <= _CDTP_GWKEY_LEN_)
            return -1;

        memcpy(buf, _pmsg->_body + _CDTP_GWKEY_LEN_ + 3, _CDTP_GWKEY_LEN_);
        buf[_CDTP_GWKEY_LEN_] = '\0';
        return 0;
    }
};

/** 응답 메시지 클래스 */
class Response : public Message {
public:
    /** 응답 메시지 생성자 */
    Response () : Message () {
    }

    /** 응답 메시지 생성자 */
    Response (Request *preq) : Message (preq->GetSNID()) {
        memcpy (_pmsg->_header, preq->GetHeader(), _CDTP_HEADERLEN_);
        (_pmsg->_header)[CDTP_HD_MSGTYPE] = CDTP_MT_RESPONSE;
        (_pmsg->_header)[CDTP_HD_RESCODE] = CDTP_RC_OK;
    }

    /** 응답 메시지 생성자 */
    Response (Request *preq, cdtprescode_t res) : Message (preq->GetSNID()) {
        memcpy (_pmsg->_header, preq->GetHeader(), _CDTP_HEADERLEN_);
        (_pmsg->_header)[CDTP_HD_MSGTYPE] = CDTP_MT_RESPONSE;
        (_pmsg->_header)[CDTP_HD_RESCODE] = (unsigned char)res;
    }

    /**
     * 응답 메시지에 응답 코드를 세팅한다.
     * @param res 메시지 응답 코드
     */
    void SetRescode (cdtprescode_t res) {
        (_pmsg->_header)[CDTP_HD_RESCODE] = (unsigned char)res;
    }

    /**
     * 응답 메시지에서 응답 코드를 꺼낸다.
     * @return 응답코드
     */
    cdtprescode_t GetRescode () {
        return (cdtprescode_t)((_pmsg->_header)[CDTP_HD_RESCODE]);
    }
};

/** Notification 메시지 클래스 */
class Notification : public Message {
public:
    /** Notification 생성자 */
    Notification() : Message() {
    }

    /** Notification 생성자 - 내부 버퍼를 별도로 세팅 */
    Notification (cdtpmsg_t *pmsg, unsigned char snid) : Message(pmsg, snid){
        (_pmsg->_header)[CDTP_HD_MSGTYPE] = CDTP_MT_NOTIFICATION;
        (_pmsg->_header)[CDTP_HD_MSGCODE] = CDTP_MC_OBSERVATION;
        (_pmsg->_body)[0] = 0;
    }

    /** Notification 생성자 */
    Notification (unsigned char snid) : Message (snid) {
        (_pmsg->_header)[CDTP_HD_MSGTYPE] = CDTP_MT_NOTIFICATION;
        (_pmsg->_header)[CDTP_HD_MSGCODE] = CDTP_MC_OBSERVATION;
        (_pmsg->_body)[0] = 0;
    }

    ~Notification () {
    }

    /** Notification 내용을 리셋한다. 기존의 메시지 재활용을 위한 방법이다. */
    void Reset() {
        (_pmsg->_body)[0] = 0;
        NextSeq ();
    }

    /**
     * Notification 메시지에 관측치를 추가한다.
     * @param sensorid 센서 아이디
     * @param value 센서 관측치
     * @return 추가 결과. 결과값은 0 일때 성공.
     */
    cdtprescode_t AddObservation(unsigned char sensorid, unsigned int value) {
        int n = (_pmsg->_body)[0];

        if (n > _CDTP_NUMOFSEN_)
            return CDTP_RC_ERR;

        (_pmsg->_body)[0] = n + 1;
        (_pmsg->_body)[n * 3 + 1] = (unsigned char) sensorid; 
        (_pmsg->_body)[n * 3 + 2] = (unsigned char) (value >> 8);
        (_pmsg->_body)[n * 3 + 3] = (unsigned char) (value & 0xFF);

        return CDTP_RC_OK;
    }

    /**
     * 메시지 내부에 있는 센서(관측치)의 개수를 얻는다.
     * @return 센서(관측치)의 개수
     */
    int GetNumberofSensors() {
        return (_pmsg->_body)[0];
    }

    /**
     * 주어진 인덱스에 따라 센서아이디를 얻는다.
     * @param idx 인덱스 값. 0에서 시작해서 최대 12가 될 수 있다.
     * @return 센서 아이디
     */
    unsigned char GetSensorID(int idx) {
        return (_pmsg->_body)[idx * 3 + 1];
    }

    /**
     * 주어진 인덱스에 따라 관측치를 얻는다.
     * @param idx 인덱스 값. 0에서 시작해서 최대 12가 될 수 있다.
     * @return 관측치
     */
    unsigned int GetObservation(int idx) {
        return ((_pmsg->_body)[idx * 3 + 2] << 8) + (_pmsg->_body)[idx * 3 + 3];
    }
};

#endif

