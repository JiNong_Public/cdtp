#-*- coding: utf-8 -*-
"""
 Copyright © 2016-2018 JiNong Inc.
 All Rights Reserved.

 CDTP python class

 If you move this file, the shared library path should be changed.

 There are three classes - CDTPBase, CDTPClient, CDTPServer.
 You don't need to used CDTPBase.
"""

import platform
import ctypes
import os

CDTP_OK = 0
""" int : response code - OK """

CDTP_ERR = 1
""" int : response code - ERR """

CDTP_MSGLEN = 64
""" int : encoded message length """

CDTP_BINMSGLEN = 48
""" int : binary message length """

CDTP_BODYLEN = 40
""" int : body length """

if platform.system() == 'Linux':
    CDTP_SO_PATH = '../build/lib/libcdtp.so'
elif platform.system() == 'Darwin':
    CDTP_SO_PATH = '../build/lib/libcdtp.dylib'
else:
    # 현재 윈도우 및 다른 OS를 지원하지 않습니다.
    CDTP_SO_PATH = ''

CDTP_SO_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), CDTP_SO_PATH)

class CDTPBase:
    """
    Base Class
    """
    def __init__(self):
        """
        CDTPBase constructor
        """
        self.cdtp = ctypes.cdll.LoadLibrary(CDTP_SO_PATH)
        self.cdtp.Req_GetGatewayKey.restype = ctypes.c_char_p
        self.cdtp.Req_GetNewGatewayKey.restype = ctypes.c_char_p
        self.cdtp._Encode.restype = ctypes.c_char_p
        self.cdtp._EncodeS.restype = ctypes.c_char_p
        self.cdtp._EncodeBinary.restype = ctypes.c_char_p
        self.cdtp._GetBinaryS.restype = ctypes.c_int
        self.cdtp._GetBinaryS.argtypes = [ctypes.c_void_p, ctypes.c_void_p]
        self.cdtp._GetCustomRequest.restype = ctypes.c_void_p
        self.cdtp._GetCustomRequest.argtypes = [ctypes.c_int, ctypes.c_ubyte, ctypes.c_void_p]

        self.cdtp._DecodeAsRequest.restype = ctypes.c_void_p
        self.cdtp._DecodeAsNotification.restype = ctypes.c_void_p
        self.cdtp._DecodeAsResponse.restype = ctypes.c_void_p
        self.cdtp._ParseAsRequest.restype = ctypes.c_void_p
        self.cdtp._ParseAsNotification.restype = ctypes.c_void_p
        self.cdtp._ParseAsResponse.restype = ctypes.c_void_p

        self.cdtp.Res_Free.argtypes = [ctypes.c_void_p]
        self.cdtp.Req_Free.argtypes = [ctypes.c_void_p]
        self.cdtp.Noti_Free.argtypes = [ctypes.c_void_p]

        self.cdtp.Msg_GetSNID.argtypes = [ctypes.c_void_p]
        self.cdtp.Msg_GetSequence.argtypes = [ctypes.c_void_p]
        self.cdtp.Req_GetGatewayKey.argtypes = [ctypes.c_void_p]
        self.cdtp.Req_GetSleepTime.argtypes = [ctypes.c_void_p]

        self.cdtp._GetRegistrationRequest.restype = ctypes.c_void_p
        self.cdtp._GetConfigurationRequest.restype = ctypes.c_void_p
        self.cdtp._GetCustomRequest.restype = ctypes.c_void_p

        self.cdtp._GetResponse.restype = ctypes.c_void_p
        self.cdtp._GetResponse.argtypes = [ctypes.c_void_p, ctypes.c_int]

        self.cdtp.Noti_GetNumberofSensors.argtypes = [ctypes.c_void_p]
        self.cdtp.Noti_GetSensorID.argtypes = [ctypes.c_void_p, ctypes.c_int]
        self.cdtp.Noti_GetObservation.argtypes = [ctypes.c_void_p, ctypes.c_int]

    def encode(self, msg):
        return self.cdtp._EncodeBinary(msg)

class CDTPServer(CDTPBase):
    """
    CDTPServer Class
    """
    def __init__(self, gwkey):
        """
        CDTPServer constructor
        Args:
            gwkey (string) : gateway key (5 characters)
        """
        CDTPBase.__init__(self) 
        self.gwkey = gwkey

    def parse(self, msg):
        """
        This function parses encoded message.
        Args:
            msg (string) : encoded message (64 characters) or not encoded message (48 characters)

        Returns:
            dictionary having parsed message.
            {'type' : 'Message Type', 'object' : reference, 'values' : dictionaray}
        """
        if len(msg) == CDTP_MSGLEN:
            noti = self.cdtp._DecodeAsNotification(msg)
        elif len(msg) == CDTP_BINMSGLEN:
            noti = self.cdtp._ParseAsNotification(msg)
        else:
            return None

        if noti:
            return self._parsenoti(noti)
        else:
            if len(msg) == CDTP_MSGLEN:
                req = self.cdtp._DecodeAsRequest(msg)
            elif len(msg) == CDTP_BINMSGLEN:
                req = self.cdtp._ParseAsRequest(msg)
            else:
                return None

            if req:
                return self._parsereq(req)
            else:
                return None

    def _parsenoti(self, noti):
        """
        This function parses Notification Message
        Args:
            noti (reference) : Notificaiton Message (Pointer)

        Returns:
            dictionary having parsed message.
            {'type' : 'Notification', 'object' : reference, 'values' : dictionaray}
        """
        n = self.cdtp.Noti_GetNumberofSensors(noti)
        obsdict = {}
        for i in range(n):
            sid = self.cdtp.Noti_GetSensorID(noti, i)
            obsdict[sid] = self.cdtp.Noti_GetObservation(noti, i)
        notidict = {'SNID' : self.cdtp.Msg_GetSNID(noti), 'observations' : obsdict}
        return {'type' : 'Notification', 'object' : noti, 'values' : notidict}

    def _parsereq(self, req):
        """
        This function parses Request Message. The gateway key in Request message should be same with the server's gateway key.
        Args:
            req (reference) : Request Message (Pointer)

        Returns:
            dictionary having parsed message.
            {'type' : 'Request', 'object' : reference, 'values' : dictionaray}
        """
        reqdict = {
            'SNID' : self.cdtp.Msg_GetSNID(req),
            'seq' : self.cdtp.Msg_GetSequence(req),
            'gatewaykey' : self.cdtp.Req_GetGatewayKey(req),
            'sleeptime' : self.cdtp.Req_GetSleepTime(req)
        }
        if reqdict['gatewaykey'] != self.gwkey:
            return None
        return {'type' : 'Request', 'object' : req, 'values': reqdict}

    def reconfigure(self, snid, gwkey, newgwkey, stime):
        """
        This function generates an encoded message for Re-configuration Request
        Args:
            snid (int) : sensor node id
            gwkey (string) : gateway key. 
            newgwkey (string) : new gateway key. if it is None, it keeps current gwkey.
            stime (int) : sleep time
        Returns:
            encoded reconfiguration message
        """
        self.cdtp._SetSNID(snid)
        req = self.cdtp._GetConfigurationRequest(snid, gwkey, newgwkey, stime)
        self.regseq = self.cdtp.Msg_GetSequence(req)
        msg = (ctypes.c_ubyte * CDTP_BINMSGLEN)()
        coded = self.cdtp._GetBinaryS(req, msg)
        self.cdtp.Req_Free(req)
        return msg

    def gencustomreq(self, msgcode, snid, body):
        """
        This function generates a custom Requst.
        Args:
            msgcode (int) : custom message code
            snid (int) : sensor node id
            body (byte array) : request body
        Returns:
            encoded custom request message
        """
        req = self.cdtp._GetCustomRequest(msgcode, snid, body)
        msg = (ctypes.c_ubyte * CDTP_BINMSGLEN)()
        coded = self.cdtp._GetBinaryS(req, msg)
        self.cdtp.Req_Free(req)
        return msg

    def response(self, parsed, rescode):
        """
        This function generates Response message from the parsed Request message.

        Args:
            parsed (dict) : parsed message. It should be a Request.
            rescode (int) : response code (0 : ok, 1 : err)
        """
        res = self.cdtp._GetResponse(parsed['object'], rescode)
        #coded = self.cdtp._EncodeS(res)
        msg = (ctypes.c_ubyte * CDTP_BINMSGLEN)()
        coded = self.cdtp._GetBinaryS(res, msg)
        self.cdtp.Res_Free(res)
        return msg 

    def freeparsed(self, parsed):
        """
        This function frees memory for a parsed message.
        It should be called after parsed function call.

        Args:
            parsed (dict) : parsed message. It should be a Request.
        """
        if parsed['type'] == 'Request':
            self.cdtp.Req_Free(parsed['object'])
        elif parsed['type'] == 'Notification':
            self.cdtp.Noti_Free(parsed['object'])

class CDTPClient(CDTPBase):
    """
    CDTPClient class
    """
    def __init__(self, gwkey, snid, sleeptime):
        """
        CDTPClient constructor
        Args:
            gwkey (string) : gateway key (5 characters)
            snid (int) : sensor node id
            sleeptime (int) : delay time (seconds)
        """
        CDTPBase.__init__(self) 
        self.gwkey = gwkey
        self.snid = snid
        self.stime = sleeptime
        self.regseq = -1 

        self.cdtp._SetSNID(snid)

    def register(self):
        """
        This function generates an encoded message for Registration Request
        Returns:
            encoded registration message
        """
        req = self.cdtp._GetRegistrationRequest(self.gwkey, self.stime)
        self.regseq = self.cdtp.Msg_GetSequence(req)
        #coded = self.cdtp._EncodeS(req)
        msg = (ctypes.c_ubyte * CDTP_BINMSGLEN)()
        coded = self.cdtp._GetBinaryS(req, msg)
        self.cdtp.Req_Free(req)
        return msg 

    def isregistered(self, msg):
        """
        This function checks a Respons message for a Registration Request.
        It checks whether reistered or not.
        Args:
            msg (string) : encoded message (64 characters) or not encoded message (48 characters)
        Returns:
            boolean. If registered then True.
        """
        if len(msg) == CDTP_MSGLEN:
            res = self.cdtp._DecodeAsResponse(msg)
        elif len(msg) == CDTP_BINMSGLEN:
            res = self.cdtp._ParseAsResponse(msg)
        else:
            return None

        if res:
            if (self.regseq == self.cdtp.Msg_GetSequence(res) and
                self.cdtp.Res_GetRescode(res) == CDTP_OK):
                ret = True
            else:
                ret = False
            self.cdtp.Res_Free(res)
            return ret
        return False

    def setobservations(self, obsdict):
        """
        This function generates a Notification Message with observations.
        Args:
            obsdict(dict) : a dictionary that key is sensor id and value is observation of the sensor
        Returns:
            encoded Notification message
        """
        noti = self.cdtp._GetNotification()
        for key, value in obsdict.iteritems():
            if self.cdtp.Noti_AddObservation(noti, key, value) != CDTP_OK:
                return None
        #coded = self.cdtp._EncodeS(noti)
        msg = (ctypes.c_ubyte * CDTP_BINMSGLEN)()
        coded = self.cdtp._GetBinaryS(noti, msg)
        self.cdtp.Noti_Free(noti)
        return msg

