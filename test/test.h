/**
 * Copyright © 2016-2018 JiNong Inc. 
 * All Rights Reserved.
 *
 * \file test.h
 * \brief 테스트하기 위해 사용되는 헤더파일
 */
#ifndef _TEST_H_
#define _TEST_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Test Run */
#define TEST(expr)											\
	do {                                                     	\
		if (0 == (expr)) {                                          \
			fprintf(stderr,                                     \
				"%s test pass in %s on line %d\n",    	\
				#expr,                                       	\
				__FILE__,                                     	\
				__LINE__);                                     	\
			fflush(stderr);                                       	\
		}                                                       \
	} while (0)

#endif 
