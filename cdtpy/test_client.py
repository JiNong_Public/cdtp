#-*- coding: utf-8 -*-
"""
 * Copyright © 2016-2018 JiNong Inc.
 * All Rights Reserved.
 *
"""

import socket
import base64
import cdtp


client = cdtp.CDTPClient("JN-01", 1, 60)
regreq = client.register()
print "Client Registration", regreq
dec = base64.b64decode(regreq)
print "decode ", str(len(dec))
print ''.join('{:02x}'.format(ord(x)) for x in dec)

csock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
csock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
csock.connect(('210.89.177.47', 10010))
print "connected."
csock.send(dec)
csock.send(dec)
print "send."
res = csock.recv(4096)
print "recv."
enc = base64.b64encode(res)
print "encoded.", enc
#parsed = client.parse(enc)
#print "Client Parse response", parsed

noti = client.setobservations({1:10, 2:20, 3:30})
print "Client Notification", noti
dec = base64.b64decode(noti)
csock.send(dec)
res = csock.recv(4096)
enc = base64.b64encode(res)
print "encoded.", enc

#parsed = client.parse(enc)
#print "Client Parse request", parsed


