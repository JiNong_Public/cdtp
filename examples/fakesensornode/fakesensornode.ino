/**
 * Copyright © 2016-2018 JiNong Inc.
 * All Rights Reserved.
 *
 * \file fakesensornode.ino
 * \brief 가짜 센서노드
 * CDTP 를 사용하는 가짜 센서노드이다. 
 * 두 가지 랜덤 센서값을 생성하여 전달한다.
 * 게이트웨이 키, 센서노드 및 2개의 센서 아이디는 #define 문으로 정의되어 있다. 
 */

#include <SoftwareSerial.h>
#include <cdtp.h>

#define NETID       {0, 0, 121, 23}     //(7917)16 = 30999 = 121, 23
#define MASTERID    {0, 0}
#define MYSNID      10

#define GWKEY       "#7001"
#define SLEEPTERM   8
#define CHECKTERM   3
#define BATPIN      3

#define ERR         -1
#define OK          0

#define RXPIN       9
#define TXPIN       10

#define MAXRTIME    5

SoftwareSerial dev(RXPIN, TXPIN);

char netid[4] = NETID;
char nodeid[2] = MASTERID;
int stime = 7;

void
writemsg(char *msg) {
    dev.write(netid, 4); 
    dev.write(nodeid, 2);
    dev.write(msg);
    dev.flush();
}

int
checkdev(int term) {
    int i;

    if (dev.available() > 0)
        return OK;

    for (i = 0; i < term; i++) {
        delay(3000);
        if (dev.available() > 0) 
            return OK;
    }

    return ERR;
}

int
readmsg(char *buf) {
    int i;

    for (i = 0; i < 64; i++) {
        buf[i] = dev.read();
        if (buf[i] == -1) {
            i--;
            continue;
        }
    }
    buf[i] = '\0';
    return OK;
}

int
readres() {
    char buf[65];

    if (checkdev(CHECKTERM) == ERR) {
        return ERR;
    }

    if (readmsg(buf) == ERR) {
        return ERR;
    }

    Response *pres = CDTP::DecodeAsResponse(buf);
    if (!pres) {
    }

    if (pres->GetRescode() == CDTP_RC_OK)
        return OK;
    else {
        return ERR;
    }
}

void
deepsleep () {
    for (int i = 0; i < stime; i++)
        delay(9000);
}

int
regnode() {
    char gwkey[] = GWKEY;
    Request *preq = CDTP::GetRegistrationRequest((unsigned char *)gwkey, SLEEPTERM * stime);
    if (!preq) {  
        return ERR;
    }

    char *pcoded = CDTP::EncodeS(preq);
    delete preq;
    if (!pcoded) {
        return ERR;
    }

    writemsg(pcoded);

    while (readres() == ERR) {
        deepsleep();
    }

    return OK;
}

int
readreq() {
    char buf[65];

    if (checkdev(0) == ERR) {
        return ERR;
    }

    if (readmsg(buf) == ERR) {
        return ERR;
    }

    Request *preq = CDTP::DecodeAsRequest(buf);
    if (preq) {
        return regnode();
    }

    return ERR;
}

void 
setup() {
    Serial.begin(9600);

    pinMode(RXPIN, INPUT);
    pinMode(TXPIN, OUTPUT);

    CDTP::SetSNID(MYSNID);

    dev.begin(9600);
    delay (2000);  

    regnode();
    randomSeed(analogRead(0));
}


void
notify () {
    float temp, hum;

    //readTemperatureAndHumidity
    temp = random(0, 30)
    hum = random(50, 100)

    Notification *pnoti = CDTP::GetNotification();
    if (!pnoti) {
        return;
    }

    pnoti->AddObservation(1, (int)(temp*10));
    pnoti->AddObservation(2, (int)(hum*10));

    char *pcoded = CDTP::EncodeS(pnoti);
    delete pnoti;
    if (!pcoded) {
        return;
    }
    writemsg(pcoded);
}

void 
loop() {
    readreq();

    notify();

    deepsleep();
}
