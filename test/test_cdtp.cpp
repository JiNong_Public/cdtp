/**
 * Copyright © 2016-2018 JiNong Inc.
 * All Rights Reserved.
 *
 * \file test_cdtp.cpp
 * \brief CDTP Test
 *
 */

#include "test.h"
#include "../cdtp.h"

#define GWKEY       "#7001"
#define NEWGWKEY    "#1235"
#define SLEEPTIME   60

int
test_register() {
    // 센서 노드 에서 등록 메세지를 만들어서 전송함
    char gwkey[] = GWKEY;
    CDTP::SetSNID(9);
    Request *preq = CDTP::GetRegistrationRequest((unsigned char *)gwkey, SLEEPTIME);
    if (!preq) {
        printf ("fail to get request.\n");
        return -1;
    }

    char *pcoded = CDTP::Encode(preq);
    delete preq;
    if (!pcoded) {
        printf ("fail to encode.\n");
        return -1;
    }
    printf ("request : %s\n", pcoded);

    // 전송했다고 가정하고, 게이트웨이에서 처리함
    preq = CDTP::DecodeAsRequest(pcoded);
    delete pcoded;

    if (!preq) {
        printf ("fail to decode as request.\n");
        return -1;
    }

    char buf[10];
    if (preq->GetGatewayKey(buf, 10) < 0) {
        printf ("fail to get gateway key.\n");
        delete preq;
        return -1;
    }
    if (strcmp (buf, GWKEY) != 0) {
        printf ("fail to match gateway key %s.\n", buf);
        delete preq;
        return -1;
    }
    if (preq->GetSleepTime() != SLEEPTIME) {
        printf ("fail to get sleep time %d.\n", preq->GetSleepTime());
        delete preq;
        return -1;
    }


    // Request에 대한 작업 완료
    Response *pres = CDTP::GetResponse(preq, CDTP_RC_OK);
    delete preq;
    if (!pres) {
        printf ("fail to get response.\n");
        return -1;
    }

    pcoded = CDTP::EncodeS(pres);
    printf ("response : %s\n", pcoded);

    pcoded = CDTP::Encode(pres);
    delete pres;
    if (!pcoded) {
        printf ("fail to encode.\n");
        return -1;
    }
    printf ("response : %s\n", pcoded);

    // 게이트웨이에서 센서노드로 응답을 전송함
    pres = CDTP::DecodeAsResponse(pcoded);
    delete pcoded;
    if (!pres) {
        printf ("fail to decode response.\n");
        return -1;
    }
    delete pres;

    return 0;
}

int
test_configure() {
    // 게이트웨이에서 재설정 메시지를 생성
    char gwkey[] = GWKEY;
    char newgwkey[] = NEWGWKEY;
    CDTP::SetSNID (1);
    Request *preq = CDTP::GetConfigurationRequest(1, (unsigned char *)gwkey, (unsigned char *)newgwkey, SLEEPTIME);
    if (!preq) {
        printf ("fail to get request.\n");
        return -1;
    }

    char *pcoded = CDTP::Encode(preq);
    delete preq;
    if (!pcoded) {
        printf ("fail to encode.\n");
        return -1;
    }
    printf ("request : %s\n", pcoded);

    // 전송했다고 가정하고, 센서노드에서 처리함
    preq = CDTP::DecodeAsRequest(pcoded);
    delete pcoded;
    if (!preq) {
        printf ("fail to decode as request.\n");
        return -1;
    }

    char buf[10];
    if (preq->GetGatewayKey(buf, 10) < 0) {
        printf ("fail to get gateway key.\n");
        delete preq;
        return -1;
    }
    if (strcmp (buf, GWKEY) != 0) {
        printf ("fail to match gateway key %s.\n", buf);
        delete preq;
        return -1;
    }
    if (preq->GetNewGatewayKey(buf, 10) < 0) {
        printf ("fail to get new gateway key.\n");
        delete preq;
        return -1;
    }
    if (strcmp (buf, NEWGWKEY) != 0) {
        printf ("fail to match new gateway key %s.\n", buf);
        delete preq;
        return -1;
    }
    if (preq->GetSleepTime() != SLEEPTIME) {
        printf ("fail to get sleep time %d.\n", preq->GetSleepTime());
        delete preq;
        return -1;
    }

    delete preq;
    // 센서노드는 응답을 전송하지 않음

    return 0;
}

int
test_notification() {
    unsigned int i;

    // 센서노드에서 노티피케이션 생성
    CDTP::SetSNID (1);
    Notification *pnoti = CDTP::GetNotification();
    if (!pnoti) {
        printf ("fail to get notification.\n");
        return -1;
    }

    for (i = 2; i < 5; i++) {
        pnoti->AddObservation(i, i * 10);
    }

    char *pcoded = CDTP::Encode(pnoti);
    delete pnoti;
    if (!pcoded) {
        printf ("fail to encode.\n");
        return -1;
    }

    printf ("Notification : %s\n", pcoded);

    // 센서노드에서 노티피케이션 전송
    // 게이트웨이에서 노티피케이션 수신 및 파싱

    pnoti = CDTP::DecodeAsNotification(pcoded);
    delete pcoded;
    if (!pnoti) {
        printf ("fail to decode as notification.\n");
        return -1;
    }

    if (pnoti->GetNumberofSensors() != 5 - 2) {
        printf ("fail to get number of sensors. %d \n", pnoti->GetNumberofSensors());
        delete pnoti;
        return -1;
    }

    for (i = 2; i < 5; i++) {
        if ((pnoti->GetSensorID(i - 2) != i) 
            || (pnoti->GetObservation(i - 2) != i * 10)) {

            printf ("fail to get id and observation. %d %d \n", 
                    pnoti->GetSensorID(i - 2), pnoti->GetObservation(i - 2));
            delete pnoti;
            return -1;
        }
    }
    delete pnoti;
    return 0;
}

int
main (int argc, char *argv[]) {
    TEST(test_register());
    TEST(test_configure());
    TEST(test_notification());
    return 0;
}
