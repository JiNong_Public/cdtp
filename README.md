# CDTP : 센서노드와 게이트웨이간 비연결형 통신 프로토콜

## Introduction

본 프로젝트는 경북대와 주식회사 지농에서 제안한 "스마트팜 센서노드와 게이트웨이간 비연결형 통신 프로토콜"을 구현한 라이브러리이다.

## 개발 환경
* 언어는 C++, python을 사용한다.
* 로컬 빌드를 위해서 cmake 를 사용한다.
* 아두이노에서의 사용도 염두에 두고 만들었으며, 아두이노용 샘플을 제공한다.
* 파이썬 코드는 공유라이브러리를 이용해서 동작한다.

## 컴파일 및 테스트
* build를 위한 폴더를 생성하고, cmake 와 make 를 실행하여 컴파일 및 테스트가 가능하다.
```
mkdir build
cd build
cmake ..
make 
make test
```

* 컴파일에 문제가 없고, 테스트가 완료되면 다음과 같은 화면을 볼 수 있다.
```
Running tests...
/usr/bin/ctest --force-new-ctest-process 
Test project /home/tombraid/Works/jinong/sfarc/cdtp/build
    Start 1: tbase64
1/2 Test #1: tbase64 ..........................   Passed    0.00 sec
    Start 2: tcdtp
2/2 Test #2: tcdtp ............................   Passed    0.00 sec

100% tests passed, 0 tests failed out of 2

Total Test time (real) =   0.01 sec
```

* 파이썬 환경에 대한 테스트는 별도로 수행할 수 있다. 단, 공유라이브러리를 사용하기 때문에 빌드가 먼저 되어 있어야 한다.

```
cd python
python test_cdtp.py
```

* 파이썬 코드 실행에 문제가 없다면 다음과 같은 화면을 볼 수 있다.
```
Client Registration BEQABAAAAAwIxIzM0EAA8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Server Parse Registration should fail None
Server Parse Registration {'values': {'SNID': 1, 'gatewaykey': '#1234', 'seq': 1, 'sleeptime': 60}, 'object': 18602160, 'type': 'Request'}
Server Resonpse BIQABAAAAAwIxIzM0EAA8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Client Registered? True
Client Notification CMQABAAAAAwABAgCCAAFDAgHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
server Parse Notification {'values': {1: 10, 2: 20, 3: 30}, 'object': 18287344, 'type': 'Notification'}
```

