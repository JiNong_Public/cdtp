#-*- coding: utf-8 -*-
"""
 * Copyright © 2016-2018 JiNong Inc.
 * All Rights Reserved.
 *
"""

import sys
import json
import socket
import base64
import daemon
import cdtp
import struct

class CDTPGateway(daemon.Runner):
    def __init__(self, conffile):
        fp = open(conffile, "r")
        self._config = json.loads(fp.read())
        fp.close()
        self._server = cdtp.CDTPServer(self._config["gatewaykey"])
        self._isrunning = False
        self._conn = None

    def parse(self, data):
        parsed = self._server.parse(data)
        if parsed is None:
            self.logger.warn("needs to data length. " + str(len(data)))
            print "needs to data length. ", str(len(data))
            return False
        return parsed

    def register(self, cur, addr):
        # read register request
        data = cur.recv(48)
        self.logger.info("reg. " + ''.join(format(ord(x), '02x') for x in data))
        parsed = self.parse(data)
        print "register request ", parsed
        if parsed is False:
            self.logger.warn("fail to parse. " + ''.join(format(ord(x), '02x') for x in data))
            return False
        if parsed['type'] != 'Request':
            self.logger.warn("message is not request. " + parsed["type"])
            return False

        self.logger.info("register request "  + str(parsed))
        print "register request ", parsed

        # should check node id
        snid = parsed["values"]["SNID"]

        res = self._server.response(parsed, cdtp.CDTP_OK)
        self._server.freeparsed(parsed)
        cur.send(res)
        return True

    def saveobservation(self, parsed):
        try:
            self.logger.info("should to save observation!!")
            self.logger.info(str(parsed["values"]["observations"]))
        except Exception as ex:
            self._conn.close()
            self.logger.warning(str(ex))
            raise ex
        return True

    def makefndmsg(self, parsed):
        try:
            self.logger.info("should to make fndmsg!!")
            self.logger.info(str(parsed["values"]["SNID"]))
            #body = (ctypes.c_ubyte * CDTP_BODYLEN)()
            payload = struct.pack('>IHHHHHHHHHHH', 0, 1, 2018, 12, 17, 16, 10, 1, 
                          12.8 * 10 + 1000, 10 * 10 + 1000, 15 * 10 + 1000, -5 * 10 + 1000)
            payload = payload + chr(0) * 18
            data = self._server.gencustomreq(0x64, parsed["values"]["SNID"], payload)
            print data[0]
            #self.logger.info("makefnd. " + ''.join(format(ord(x), '02x') for x in data))
            return data
        except Exception as ex:
            self._conn.close()
            self.logger.warning(str(ex))
            raise ex

    def observation(self, cur, addr):
        # read notification
        data = cur.recv(48)
        self.logger.info("obs. " + ''.join(format(ord(x), '02x') for x in data))
        parsed = self.parse(data)
        if parsed is False:
            return False
        if parsed['type'] != 'Notification':
            self.logger.warn("message is not Notification. " + parsed["type"])
            return False

        self.logger.info("notification "  + str(parsed))

        # should check node id
        snid = parsed["values"]["SNID"]

        # save observation
        self.saveobservation(parsed)

        # make observation
        req = self.makefndmsg(parsed)
        self._server.freeparsed(parsed)
        cur.send(req)
        return True

    def close(self):
        self._conn.close()

    def connect(self):
        self._conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._conn.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._conn.bind(('0.0.0.0', self._config["network"]["port"]))
        self._conn.listen(10)

    def accept(self):
        cur, addr = self._conn.accept()
        self.logger.info("connected from " + str(addr))
        if self.register(cur, addr):
            self.observation(cur, addr)
        #cur.shutdown(1)
        cur.close()

    def run(self):
        self._isrunning = True
        while self._isrunning:
            try:
                self.connect()
                while self._isrunning:
                    self.accept()
            except Exception as ex:
                self.logger.warn(str(ex))
                self.close()

    def stop(self):
        self._isrunning = False

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage : python test_server.py [start|stop|restart|run]"
        sys.exit(2)

    mode = sys.argv[1]
    runner = CDTPGateway("default.conf")
    adaemon = daemon.Daemon("HS_AWS", runner)
    if 'start' == mode:
        adaemon.start()
    elif 'stop' == mode:
        adaemon.stop()
    elif 'restart' == mode:
        adaemon.restart()
    elif 'run' == mode:
        adaemon.run()
    else:
        print "Unknown command"
        sys.exit(2)
    sys.exit(0)

